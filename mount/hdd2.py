import json
import subprocess
import time

from config.config import IO
from utils.utils import Utils

BORDER = "======================================================="


class Partition:
    def __init__(self, serial, partition, parent, mount_point=""):
        """
        sudo blkid
        """
        self.parent = parent
        self.serial = serial
        self.uuid = self.get_uuid(partition)
        self.uid = serial + ':' + self.uuid
        self.mount_point = mount_point
        self.partition = partition
        self.last_update = time.time()

        self.size = 0
        self.used = 0
        self.available = 0
        self.percentage = 0

        self.role = "unconf"
        self.path = "/"         # directly set after parsing and checking

    def update(self, partition):
        self.parent = partition.parent
        self.serial = partition.serial
        self.uid = partition.uid
        self.mount_point = partition.mount_point
        self.partition = partition.partition
        self.last_update = time.time()

        self.get_size()

    @staticmethod
    def get_uuid(part_path):
        cmd = "blkid"
        output = Utils.subprocess_cmd(cmd)

        for out in output:  # type: bytes
            data = out.decode().split()
            if part_path in data[0]:
                for entry in data:
                    if entry.startswith('PARTUUID="'):
                        uuid = entry.replace('PARTUUID="', '').replace('"', '')
                        # print(uuid)
                        return uuid
        return None

    def get_size(self):
        cmd = "df -Pk " + self.partition + " | sed -e /^Filesystem/d | awk '{printf \"%s:%s:%s:%s\\n\", $2,$3,$4,$5}'"
        # cmd = "df -Ph " + self.partition + " | sed -e /^Filesystem/d"
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        output = process.communicate()

        for out in output:
            if out is not None:
                data = str(out)[2:-3].split(":")
                self.size = int(data[0])
                self.used = int(data[1])
                self.available = int(data[2])
                self.percentage = int(data[3][0:-1])

    def parse_path(self, rel_dir_path):
        if rel_dir_path[0] != '/':
            rel_dir_path = '/' + rel_dir_path

        abs_path = self.mount_point + rel_dir_path
        if abs_path[-1] != '/':
            abs_path += '/'

        os_path = IO.parse_path(abs_path)

        if IO.is_dir(os_path):
            return os_path

        return None

    def parse_backup_path(self, serial: str, rel_dir_path: str):
        if rel_dir_path[0] != '/':
            rel_dir_path = '/' + rel_dir_path

        # abs_path = self.mount_point + '/' + serial + rel_dir_path
        abs_path = self.mount_point + '/' + serial
        if abs_path[-1] != '/':
            abs_path += '/'

        os_path = IO.parse_path(abs_path)

        IO.mkdir(os_path)

        if IO.is_dir(os_path):
            return os_path

        return None

    def print(self):
        print(self.partition, self.mount_point, self.size, self.used)


class HDD:
    def __init__(self, serial="", device_node=""):
        self.device_node = device_node
        self.serial = serial
        self.partitions = []

        self.size = 0
        self.used = 0
        self.available = 0
        self.percentage = 0

        self.last_connected = time.time()

        self.last_update = time.time()

    def update(self, hdd):
        self.device_node = hdd.device_node
        self.get_size()
        self.last_update = time.time()

    def add_partition(self, partition: Partition):
        index = self.index_of_partition(partition.uuid)

        if index < 0:
            self.partitions.append(partition)
        else:
            self.partitions[index] = partition

    def index_of_partition(self, uuid: str) -> int:
        i = 0
        for part in self.partitions:    # type: Partition
            if part.uuid == uuid:
                return i
            i += 1

        return -1

    def get_size(self):
        self.size = 0
        self.used = 0
        self.available = 0
        self.percentage = 0

        for partition in self.partitions:
            # partition.get_size()
            self.size += partition.size
            self.used += partition.used
            self.available += partition.available
            self.percentage += partition.percentage

    def get_partition_by_uid(self, uid):
        uuid = uid.split(':')[1]
        return self.get_partition_by_uuid(uuid=uuid)

    def get_partition_by_uuid(self, uuid):
        for partition in self.partitions:
            if uuid == partition.uuid:
                return partition
        return None

    def get_partition_by_name(self, name):
        for partition in self.partitions:
            p_name = Utils.path_to_name(partition.mount_point)
            if name == p_name:
                return partition
        return None

    def print(self):
        print(BORDER)

        print(self.to_json_string())

        print(BORDER, "\n")

    def to_json_string(self) -> str:
        return json.dumps(self, default=lambda o: o.__dict__, indent=4)

    def load_json_string(self, json_string: str):
        self.__dict__ = json.loads(json_string)







