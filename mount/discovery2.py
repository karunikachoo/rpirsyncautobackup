import threading
import time

from pyudev import Device
import pyudev

from config.config import Config
from messages.handler import CMD_SUCCESS
from mount.hdd2 import HDD, Partition
from rsync.backup import Backup
from utils.utils import Utils


class Discovery(threading.Thread):
    UPDATE_TIME = 30
    SLEEP_TIME = 1

    def __init__(self, main):
        super(Discovery, self).__init__()

        self.main = main

        self.partitions = {}
        self.hdds = {}

        self.to_sync = {}

        self.alive = threading.Event()
        self.alive.set()

        self.context = pyudev.Context()

    def run(self):

        while self.alive.isSet():
            self.check_udev()

            self.lsblk_part()

            self.check_umount()
            self.check_detach()

            time.sleep(self.SLEEP_TIME)

    # region HDD

    def check_udev(self):
        devices = [dev for dev in self.context.list_devices(
            subsystem='block',
            DEVTYPE='disk') if dev['MAJOR'] == '8']

        self.check_devices(devices)

    def check_devices(self, devices):
        dev_serials = []
        remove_serials = []

        for device in devices:
            if 'ID_SERIAL' not in device:
                return
            serial = device['ID_SERIAL']
            dev_serials.append(serial)

            self.get_device(device)

        for serial in self.hdds.keys():
            if serial not in dev_serials:
                remove_serials.append(serial)

        for serial in remove_serials:
            self.hdds.pop(serial)

    def get_device(self, device: Device) -> HDD:
        serial = device['ID_SERIAL']
        hdd = HDD(serial=serial, device_node=device.device_node)

        if serial not in self.hdds:
            self.hdds[serial] = hdd

        else:
            self.hdds[serial].update(hdd)
        return hdd

    def update_hdd(self, hdd: HDD):
        for uuid in self.partitions:
            part = self.partitions.get(uuid)
            if part.parent == hdd.device_node:
                hdd.add_partition(part)

        hdd.get_size()

    def update_hdds(self):
        for serial in self.hdds:
            hdd = self.hdds.get(serial)
            self.update_hdd(hdd)
    # endregion

    # region GET PARTITION
    def lsblk_part(self, now=False):
        """
        Checks for newly mounted partitions and handles them
        :return:
        """
        cmd = "lsblk -rbpn -o NAME,SIZE,PKNAME,TYPE,MOUNTPOINT -I 8 | grep -E 'part'"
        output = Utils.subprocess_cmd(cmd)

        uuids = []

        for out in output:  # type: bytes
            part = out.decode().split()
            node = part[0]
            size = part[1]
            parent = part[2]

            if len(part) == 5:          # means mount_point has been set
                mount_path = part[4]
                # print(node, size, parent, mount_path)
                uuid = Partition.get_uuid(node)
                uuids.append(uuid)

                self.check_partition(uuid, node, parent, mount_path, now=now)

        self.remove_partitions(uuids)

    def check_partition(self, uuid, node, parent, mount_path, now=False):
        hdd = self.get_hdd_by_node(parent)  # type: HDD

        if uuid not in self.partitions:     # new partition
            print(':: New Partition ::')
            part = self.new_partition(hdd.serial, node, parent, mount_path)
            self.partitions[part.uuid] = part

            self.update_hdds()
            self.print()
            self.notify_device_change()

            self.check_sync(part)

        else:
            part = self.partitions.get(uuid)
            if self.is_part_updatable(part) or now:
                print(':: Updating Partition ::')
                new_part = self.new_partition(hdd.serial, node, parent, mount_path)
                part.update(new_part)

                self.update_hdds()
                self.print()
                self.notify_device_change()

    def remove_partitions(self, uuid_list: list):
        remove_list = []

        for uuid in self.partitions.keys():
            if uuid not in uuid_list:
                remove_list.append(uuid)

        for uuid in remove_list:
            print(':: Partition Removed ::')
            self.partitions.pop(uuid)

            self.print()
            self.notify_device_change()

        self.update_hdds()

    def new_partition(self, serial, node, parent, mount_path):
        part = Partition(serial=serial, partition=node, parent=parent, mount_point=mount_path)
        part.get_size()
        self.check_partition_config(part)

        return part

    def is_part_updatable(self, part: Partition):
        now = time.time()
        return now - part.last_update >= self.UPDATE_TIME

    def check_partition_config(self, partition: Partition):
        uid = partition.uid

        print('uid: ' + uid)

        if uid in self.main.model.config.roles.keys():
            partition.role = self.main.model.config.roles[uid]
        else:
            partition.role = Config.UNCONF

        if uid in self.main.model.config.paths.keys():
            partition.path = self.main.model.config.paths[uid]
        else:
            partition.path = '/'

    def check_sync(self, partition: Partition):
        print(":: CHECKING SYNC ::")
        is_target = self.main.model.config.is_partition_target(partition)
        print(partition.mount_point, ": is target :", is_target)
        if is_target:
            backups = self.get_backup_partitions()
            for backup in backups:
                backup = Backup(self.main, partition, backup)

                self.to_sync[partition.uuid] = backup

                self.main.rsync_handler.put(backup)
    # endregion

    # region GETTERS
    def get_partitions_by_node(self, device_node):
        ret = []
        for uuid in self.partitions:
            part = self.partitions.get(uuid)  # type: Partition
            if part.parent == device_node:
                ret.append(part)

        return ret

    def get_backup_partitions(self):
        ret = []
        for uid in self.main.model.config.get_backup_uids():
            ret.append(self.get_partition_by_uid(uid))

        return ret

    def get_target_partitions(self):
        ret = []
        for uid in self.main.model.config.get_target_uids():
            ret.append(self.get_partition_by_uid(uid))

        return ret

    def get_hdd_by_node(self, node):
        for serial in self.hdds:
            hdd = self.hdds.get(serial)
            if hdd.device_node == node:
                return hdd

        return None

    def get_partition_by_uid(self, uid: str):
        uuid = uid.split(':')[1]

        return self.partitions.get(uuid)

    def get_hdd_list(self):
        ret = []

        for serial in self.hdds.keys():
            hdd = self.hdds.get(serial)
            ret.append(hdd.to_json_string())

        return ret

    def get_sync_hdd_serials(self) -> list:
        ret = []
        for uuid in self.to_sync:
            backup = self.to_sync.get(uuid)  # type: Backup
            ret.append(backup.partition.serial)

        return ret
    # endregion

    # region BACKUP
    def backup_failed(self, backup: Backup):
        self.backup_complete(backup)

    def backup_complete(self, backup: Backup):
        remove_uuid = None
        for uuid in self.to_sync:
            s_backup = self.to_sync.get(uuid)  # type: Backup
            if s_backup.guid == backup.guid:
                remove_uuid = uuid

        if remove_uuid is not None:
            self.to_sync.pop(remove_uuid)

    def check_for_umount(self, partition: Partition) -> bool:
        """
        checks if still queued for syncing,
        if sister partitions are queued for sync

        :param partition:
        :return:
        """

        if self.to_sync is None or partition is None:
            return False

        if partition.uuid in self.to_sync:
            # partition still set to sync
            return False

        # check self and sister partitions:
        hdd = self.hdds.get(partition.serial)  # type: HDD
        for part in hdd.partitions:
            if part.uuid in self.to_sync:
                return False

        for part in hdd.partitions:  # type: Partition
            if not Utils.umount(part.mount_point):
                return False

        return True

    def check_detach(self):
        for serial in self.hdds:
            hdd = self.hdds.get(serial)  # type: HDD
            now = time.time()
            if now - hdd.last_connected > 30:   # if more than 30s and nothing mounted then detach

                parts = self.get_partitions_by_node(hdd.device_node)
                if len(parts) == 0:
                    print(':: DETACHING DEVICE :: ' + hdd.device_node)
                    Utils.detach(hdd.device_node)

    def check_umount(self):
        for partition in self.get_target_partitions():
            if partition is not None:
                hdd = self.get_hdd_by_node(partition.parent)
                now = time.time()
                if now - hdd.last_connected > 30:
                    if self.check_for_umount(partition):
                        self.main.msg_handler.broadcast('hdd-umount', CMD_SUCCESS)
    # endregion

    # region MISC
    def print(self):
        print(">>", time.time())
        for serial in self.hdds.keys():
            hdd = self.hdds.get(serial)
            hdd.print()

    def notify_device_change(self):
        print(':: broadcasting :: Device Changed')
        hdds = self.get_hdd_list()
        self.main.msg_handler.broadcast(cmd='get_hdds', args=hdds)
    # endregion
