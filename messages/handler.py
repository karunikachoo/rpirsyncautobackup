import base64

from flask_socketio import SocketIO

from utils.utils import Utils

CMD_SUCCESS = 100
CMD_ERR = 200


class MessageHandler:
    def __init__(self, app, main, socketio):
        self.main = main
        self.app = app
        self.socketio = socketio  # type: SocketIO

        self.clients = {}

    def add_client(self, token, sid):
        self.clients[token] = sid

    def get_client(self, token):
        if token in self.clients:
            return self.clients[token]
        else:
            return None

    def raw_emit(self, token, event, data):
        self.socketio.emit(event, data, namespace='/', room=token)

    def reply(self, token, cmd, args):
        data = {
            'cmd': cmd,
            'args': args
        }
        self.socketio.emit('py-cmd', data, namespace='/', room=token)

    def broadcast(self, cmd, args):
        data = {
            'cmd': cmd,
            'args': args
        }
        self.socketio.emit('py-cmd', data, namespace='/', broadcast=True)

    def token_err(self, token, cmd):
        self.socketio.emit('TOKEN_ERR', cmd, room=token)

    """
    HANDLER MAIN
    """

    def handle(self, data):
        token = data['token']

        cmd = data['cmd']

        if not self.main.auth.verify_token(token):
            print(token, ': ', self.main.auth.verify_token(token))
            self.token_err(token, cmd)
            return

        if cmd == "get_hdds":
            self.get_hdds(data)
            return
        elif cmd == "get_config_ids":
            self.get_config_ids(data)
            return
        elif cmd == "sign-in":
            self.sign_in(token, data)
            return
        elif cmd == "hdd-sync-state":
            self.hdd_sync_state()
            return

        """
        CMDs THAT NEED AUTH
        """
        if not self.main.auth.verify_token_auth(token):
            self.token_err(token, cmd)
            return

        if cmd == 'hdd-umount':
            self.hdd_umount(data)
        elif cmd == 'hdd-config':
            self.hdd_config(data)
        elif cmd == 'shutdown':
            self.shutdown(data)

    """
    ==============
    == HANDLERS ==
    ==============
    """
    def get_hdds(self, data):
        token = data['token']
        cmd = data['cmd']
        # args = data['args']

        hdds = self.main.discovery.get_hdd_list()
        self.reply(token=token, cmd=cmd, args=hdds)

    def get_config_ids(self, data):
        cmd = data['cmd']
        # args = data['args']

        # self.reply(cmd=cmd, args="")

    def sign_in(self, token, data):
        cmd = data['cmd']
        args = data['args']

        uname = base64.b64decode(args['uname']).decode()
        pw = base64.b64decode(args['pw']).decode()

        res = self.main.auth.verify_pw(token, uname, pw)

        if res:
            self.raw_emit(token, 'go_to', '/index')
        else:
            self.token_err(token, cmd)

        print(uname, pw, res)

    def hdd_sync_state(self):
        serials = self.main.discovery.get_sync_hdd_serials()

        self.broadcast('hdd-sync-state', serials)

        """"""

    def hdd_umount(self, data):
        token = data['token']
        cmd = data['cmd']
        args = data['args']

        print(args)

        hdd = self.main.discovery.hdds[str(args)]
        for partition in hdd.partitions:
            Utils.umount(partition.mount_point)

        self.main.discovery.lsblk_part(now=True)

        self.reply(token, cmd, CMD_SUCCESS)

    def hdd_config(self, data):
        token = data['token']
        cmd = data['cmd']
        args = data['args']

        roles = args['roles']       # type: dict
        paths = args['paths']       # type: dict

        for uid in roles.keys():
            role = roles.get(uid)
            # update config
            self.main.model.config.roles[uid] = role

            # update hdd
            partition = self.main.discovery.get_partition_by_uid(uid)

            partition.role = role

        for uid in paths.keys():
            path = paths.get(uid)
            # update config
            self.main.model.config.paths[uid] = path

            # update hdd
            partition = self.main.discovery.get_partition_by_uid(uid)

            partition.path = path

        self.main.model.save()

        self.reply(token, cmd, CMD_SUCCESS)
        self.main.discovery.notify_device_change()

    def shutdown(self, data):
        cmds = ['sudo shutdown now']
        Utils.sys_run(cmds)

