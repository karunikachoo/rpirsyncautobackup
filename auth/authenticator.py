import time
import bcrypt

from config.config import IO
from utils.utils import Utils


class Auth:
    AUTH_TIMEOUT = 300
    CONN_TIMEOUT = 500

    def __init__(self):
        self.connections = {}
        self.authenticated = {}

        self.last_ts = 0

    def generate_token(self) -> str:
        token = Utils.generate_random(32)

        self.new_conn(token)

        return token

    def verify_pw(self, token: str, uname: str, pw: str) -> bool:
        if token in self.connections:
            if self.auth(uname=uname, pw=pw):
                self.add_auth(token)
                return True
        return False

    def verify_token(self, token):
        return token in self.connections

    def verify_token_auth(self, token):
        return token in self.authenticated

    def check_tokens(self):

        if len(self.authenticated) > 30 or len(self.connections) > 30:
            self.authenticated = {}
            self.connections = {}
            return

        auth_removal = []
        conn_removal = []

        now = time.time()

        if now - self.last_ts < 10:
            return

        for token in self.authenticated.keys():
            ts = self.authenticated.get(token)
            if now - ts > self.AUTH_TIMEOUT:
                auth_removal.append(token)
                conn_removal.append(token)

        for token in self.connections.keys():
            ts = self.connections.get(token)
            if now - ts > self.CONN_TIMEOUT:
                conn_removal.append(token)

        for token in auth_removal:
            self.remove_auth(token)
            self.remove_conn(token)

        for token in conn_removal:
            self.remove_conn(token)

        self.last_ts = now
        # print(self.connections)

    def new_conn(self, uid):
        self.connections[uid] = time.time()

    def remove_conn(self, uid):
        if uid in self.connections:
            self.connections.pop(uid)

    def add_auth(self, uid):
        if uid in self.connections:
            self.authenticated[uid] = time.time()

    def remove_auth(self, uid):
        if uid in self.authenticated:
            self.authenticated.pop(uid)

    """
    USERNAME PASSWORD AUTHENTICATION
    """
    def auth(self, uname: str, pw: str) -> bool:
        hashed = IO.read_file(uname + '.auth')

        return self.check(pw.encode(), hashed.encode())

    def generate(self, uname: str, pw: str):
        IO.write_file(uname + '.auth', self.hash(pw.encode()))

    @staticmethod
    def hash(password: bytes) -> str:
        # Hash a password for the first time, with a randomly-generated salt
        hashed = bcrypt.hashpw(password, bcrypt.gensalt())

        return hashed.decode()

        # Check that an unencrypted password matches one that has
        # previously been hashed

    @staticmethod
    def check(password: bytes, hashed: bytes):
        if bcrypt.hashpw(password, hashed) == hashed:
            print("It matches")
            return True
        else:
            print("It does not match")
            return False




