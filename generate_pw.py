import sys

from auth.authenticator import Auth
from config.config import IO, ConfigModel

"""
RUN THIS AS FOLLOWS:

sudo python3 generate_pw.py username password

username and password will be up to you to decide
"""

if __name__ == "__main__":
    IO.switch_working_dir(ConfigModel.WORKING_DIR)
    auth = Auth()
    auth.generate(str(sys.argv[1]), str(sys.argv[2]))