let cookie = null;
let cmdHandler = null;
let helloCallback = null;

let socket = io.connect('http://' + document.domain + ':' + location.port);

socket.on('connect', () => {
    cookie = window.getCookie('uid');
    console.log("Cookie: " + cookie);
    socket.emit('py-hello', cookie);
});

socket.on('py-hello', () => {
    if (helloCallback != null) {
        helloCallback()
    }
});

socket.on('TOKEN_ERR', (cmd) => {
    console.error('TOKEN_ERR', cmd);
    location.href = 'http://' + document.domain + ':' + location.port;
});

socket.on('go_to', (addr) => {
    setTimeout(function(){document.location.href = addr}, 50);
});

const CMD_SUCCESS = 100;
const CMD_ERR = 200;

let request = (cmd, args) => {
    let data = {
        'token': cookie,
        'cmd': cmd,
        'args': args
    };
    socket.emit('py-cmd', data);
};

socket.on('py-cmd', (data) => {
    if (cmdHandler != null) {
        let cmd  = data['cmd'];
        let args = data['args'];
        cmdHandler(cmd, args);
    }
});

window.getCookie = function(name) {
  let match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
  if (match) return match[2];
};