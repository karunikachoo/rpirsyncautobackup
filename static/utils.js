let sizeMap = {
  1: 'k',
  2: 'M',
  3: 'G',
  4: 'T',
  5: 'P',
  6: 'E'
}

function calcSize(rawBytes) {
  let byteStr = rawBytes.toString();
  let pos = Math.ceil(byteStr.length / 3);
  let size = rawBytes/(1000**(pos-1));
  let postfix = sizeMap[pos] + "B";

  return {'size': size, 'postfix': postfix};
}

function parsePath2Name(path) {
  let parts = path.split('/');
  return parts[parts.length - 1]
}

function simplifySerial(serial) {
    let parts = serial.split('_');
    return parts[parts.length - 1]
}

class PopUp {
    constructor(title, sub, msg) {
        this.ref = document.querySelector('.prog-popup');
        this.title = this.ref.querySelector('.title');
        this.sub = this.ref.querySelector('.subtitle');
        this.msg = this.ref.querySelector('.subcard');

        this.close = this.ref.querySelector('.btn-edge#close-prog');

        this.title.innerText = title;
        this.sub.innerText = sub;
        this.msg.innerText = msg;

        this.close.onclick = () => {
            this.hide()
        }
    }

    show() {
        this.ref.style.display = "flex";
    }

    hide() {
        this.ref.style.display = "none";
    }

}

class HddPopUp {
    constructor(hdd) {
        this.hdd = hdd;
        this.ref = document.querySelector(".hdd-popup");
        this.partitions = this.ref.querySelector('.partitions');
        this.serialView = this.ref.querySelector(".serial");
        this.umount = this.ref.querySelector(".btn.umount");
        this.save = this.ref.querySelector(".btn.save");
        this.close = this.ref.querySelector('.btn-edge#close-popup');

        this.serial = hdd['serial'];
        this.serialView.innerText = this.serial;

        this.umount.onclick = () => {
            this.unmountHDD();
        };

        this.save.onclick = () => {
            this.saveHDDConfigs();
        };

        this.close.onclick = () => {
            this.hide();
        };

        this.partitions.innerHTML = "";

        for (let i=0; i < hdd['partitions'].length; i++) {
            this.addPartition(hdd['partitions'][i]);
        }

        this.selects = this.ref.querySelectorAll('select');
        this.paths = this.ref.querySelectorAll('input[name="bpath"]');
    }

    addPartition(partition) {
        let name = parsePath2Name(partition['mount_point']);
        let size = calcSize(partition['size']);
        let free = calcSize(partition['available']);
        let uuid = partition['uuid'];
        let title = name + ' (' + size.size.toFixed(2) + ' ' + size.postfix + ')';

        let html = '<div class="title">' + title + '</div>' +
            '<div class="free">' + partition['percentage'] + '% usage with ' + free.size.toFixed(2) + ' ' + free.postfix + ' free</div>' +
            '<div class="subtitile">Use As</div>' +
            '<div class="subcard">' +
                '<label>' +
                    '<select id="' + uuid + '">' +
                        '<option value="unconf">None</option>' +
                        '<option value="backups">Backup To</option>' +
                        '<option value="target">Backup From</option>' +
                    '</select>' +
                '</label>' +
            '</div>' +

            '<div class="subtitile">Backup Path</div>'  +
            '<div class="subcard">' +
                '<label>' +
                    '<input type="text" id="' + uuid + '" name="bpath" value="' + partition['path'] + '">' +
                '</label>' +
            '</div>';

        let part = document.createElement('div');
        part.classList.add('partition');
        part.id = uuid;
        part.innerHTML = html;

        part.querySelector('select').value = partition['role'];

        this.partitions.appendChild(part);
    }

    show() {
        this.ref.style.display = "flex";
    }

    hide() {
        this.ref.style.display = "none";
    }

    unmountHDD() {
        let cfmStr = 'Are you sure you want to eject this HDD? \r\nPlease make sure all processes are completed before ejecting...';
        if (window.confirm(cfmStr)) {
            console.log("Unmount: " + this.serial);
            request('hdd-umount', this.serial);

            this.hide();
        } else {

        }
    }

    saveHDDConfigs() {
        // console.log(this.selects);
        // console.log(this.paths);

        let data = {
            'roles': {},
            'paths': {}
        };

        for (let i = 0; i < this.selects.length; i++) {
            let el = this.selects[i];

            let uid = this.serial + ':' + el.id;
            data['roles'][uid] = el.options[el.selectedIndex].value;
        }

        for (let i = 0; i < this.paths.length; i++) {
            let el = this.paths[i];

            let uid = this.serial + ':' + el.id;
            data['paths'][uid] = el.value;
        }

        console.log(data);

        request('hdd-config', data);

        // let option = this.selection.options[this.selection.selectedIndex].value;

        // console.log("Selection: " + option);
    }
}