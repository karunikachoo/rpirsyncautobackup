import queue
import threading
import time

from messages.handler import CMD_SUCCESS, CMD_ERR
from rsync.backup import Backup
from utils.utils import Utils


class RsyncHandler(threading.Thread):
    def __init__(self, main):
        super(RsyncHandler, self).__init__()
        self.main = main

        self.rsync_queue = queue.Queue()

        self.alive = threading.Event()
        self.alive.set()

        self.sleep_duration = 0.2

    def put(self, backup: Backup):
        self.rsync_queue.put(backup)

    def run(self):
        while self.alive.isSet():
            try:
                backup = self.rsync_queue.get(True, self.sleep_duration)
                self.handle_backup(backup)
            except queue.Empty as e:
                continue

    def handle_backup(self, backup: Backup):
        success, err = backup.start()

        time.sleep(2)
        if success:
            print(err)
            """"""
            # TODO: Notify web
            self.main.msg_handler.broadcast('sync-state', CMD_SUCCESS)
            self.main.discovery.backup_complete(backup)
        else:
            """"""
            print(err)
            # TODO: Notify Web, Log Err
            self.main.msg_handler.broadcast('sync-state', CMD_ERR)
            self.main.discovery.backup_failed(backup)

