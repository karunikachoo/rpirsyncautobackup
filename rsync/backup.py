from mount.hdd import HDD, Partition
from rsync.rsync_wrapper import Rsync
from utils.utils import Utils


class Backup:

    def __init__(self, main, target_partition: Partition, backup_partition: Partition):
        self.guid = Utils.generate_random(16)
        self.main = main
        self.partition = target_partition
        self.backup = backup_partition
        self.rel_paths = self.split_paths(target_partition.path)
        self.size = 0

    def check_size(self) -> (int, dict):
        self.size = 0

        err_path = {}

        self.backup.get_size()
        for rel_path in self.rel_paths:
            target_path = self.partition.parse_path(rel_path)
            backup_path = self.backup.parse_backup_path(self.partition.serial, rel_path)

            print('CHECK PATHS', target_path, backup_path)

            if target_path is not None and backup_path is not None:
                size = Rsync.dry_run(target_path, backup_path)
                self.size += size
            else:
                err_path[self.partition.uid] = rel_path

        print(self.size, self.backup.available, str(self.size / self.backup.available * 100)[0:5], '%',
              self.size < self.backup.available, err_path)

        if (0 < self.size < self.backup.available) and len(err_path.keys()) == 0:
            return 1, err_path

        if self.size == 0:
            return 0, err_path

        return -1, err_path

    def sync(self) -> (bool, dict):
        """"""
        err_path = {}
        for rel_path in self.rel_paths:
            target_path = self.partition.parse_path(rel_path)
            backup_path = self.backup.parse_backup_path(self.partition.serial, rel_path)

            print('SYNC PATHS', target_path, backup_path)

            if target_path is not None and backup_path is not None:
                size = Rsync.sync(target_path, backup_path, callback=self.sync_update)
                print("SYNCED: ", size)
            else:
                err_path[self.partition.uid] = rel_path

        if len(err_path.keys()) == 0:
            return True, err_path
        else:
            return False, err_path

    def sync_update(self, cur, totf, target, backup):
        print('SYNC PROG: ', cur, '/', totf)
        self.main.msg_handler.broadcast('sync-prog', {
            'target': target,
            'backup': backup,
            'current': cur,
            'total': totf
        })

    @staticmethod
    def split_paths(paths: str):
        paths_arr = paths.split(';')
        ret = []
        for path in paths_arr:
            ret.append(path.strip())
        return ret

    def parse_paths(self, paths: str):
        paths_arr = paths.split(';')
        ret = []
        for path in paths_arr:
            os_path = self.partition.parse_path(path.strip())
            ret.append(os_path)
            print(os_path)
        return ret

    def start(self):
        state, err = self.check_size()
        if state == 1:
            print(":: Sufficient Size :: Backing up...")
            return self.sync()
        elif state == -1:
            print(":: Insufficient storage :: Aborting backup...")
            return state, err
        elif state == 0:
            print(":: No Changes detected :: Aborting backup...")
            return state, err

