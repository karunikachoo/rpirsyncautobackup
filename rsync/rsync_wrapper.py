import re
import subprocess

from config.config import IO


class Rsync:
    def __init__(self):
        """
        Check if device has been setup etc
        """

    @staticmethod
    def dry_run(target, backup):
        """
        sudo rsync -rlptgoDvus --modify-window=1 --delete --dry-run --inplace '/media/pi/Storage/3ds' '/media/pi/Transcend/ST1000LM024_HN-M101MBB_S35TJ9CG301527' --stats
        :param target:
        :param backup:
        :return:
        """
        if not IO.is_dir(IO.parse_path(target)):
            return -1

        IO.mkdir(IO.parse_path(backup))

        cmd = ["sudo rsync -rlptgoDvus --modify-window=1 --delete --dry-run --inplace '" + target + "' '" + backup + "' --stats | grep 'Total transferred file size:' | awk '{print $5}'"]
        # cmd = ["sudo rsync -rlptgoDvus --modify-window=1 --delete --dry-run --inplace '" + target + "' '" + backup + "' --stats"]
        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)

        output = proc.communicate()

        for out in output:
            line = str(out)[2:-3].strip().replace(',', '')

            return int(line)/1000

    @staticmethod
    def sync(target, backup, callback=None):
        if not IO.is_dir(IO.parse_path(target)):
            return -1

        IO.mkdir(IO.parse_path(backup))

        cmd = [
            "sudo rsync -rlptgoDvus --modify-window=1 --delete --dry-run --inplace '" + target + "' '" + backup + "' --stats"]

        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)

        output = proc.communicate()
        mn = re.findall(r'Number of regular files transferred: (\d+)', output[0].decode())
        total_files = int(mn[0])

        print('total_files: ', total_files)

        cmd = [
            "sudo rsync -rlptgoDvus --modify-window=1 --delete --inplace --progress '" + target + "' '" + backup + "'"]
        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stdin=subprocess.PIPE)

        i = 0

        while proc.poll() is None:
            output = proc.stdout.readline()
            line = output.decode()
            if '100%' in line:

                i += 1

                progress = i / total_files * 100

                if callback is not None:
                    callback(i, total_files, target, backup)

                # self.main.msg_handler.broadcast('sync-prog', {
                #     'target': target,
                #     'backup': backup,
                #     'prog': progress
                # })
                if i == total_files:
                    break
            elif '%' in line:
                print(line)

        # output = proc.communicate()
        # for out in output:
        #     line = str(out)[2:-3].replace(',', '')
        #
        #     return int(line)/1000
