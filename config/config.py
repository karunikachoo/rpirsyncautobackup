import errno
import json
import os
import subprocess
from pathlib import Path

TAG = "Config: "


class Config:
    BACKUP = 'backups'
    TARGET = 'target'
    UNCONF = 'unconf'

    def __init__(self):
        self.roles = {}
        self.paths = {}
        self.last_backups = {}

    def get_backup_uids(self):
        ret = []
        for uid in self.roles.keys():
            if self.roles.get(uid) == Config.BACKUP:
                ret.append(uid)
        return ret

    def is_partition_target(self, partition) -> bool:
        return partition.uid in self.get_target_uids()

    def get_target_uids(self):
        ret = []
        for uid in self.roles.keys():
            if self.roles.get(uid) == Config.TARGET:
                ret.append(uid)
        return ret

    def set_path(self, uid, path):
        self.paths[uid] = path

    def to_json_string(self) -> str:
        return json.dumps(self, default=lambda o: o.__dict__, indent=4)

    def load_json_string(self, json_string: str):
        self.__dict__ = json.loads(json_string)

    def print(self):
        print("========= CONFIG =========")
        print(self.to_json_string())


class IO:
    @staticmethod
    def write_file(file_name: str, data: str):
        """Method to write data string to a file

        You can also specify the path in the file_name, see example below.

        Example:
            write_file("/home/pi/mcbus2/file.json", "<data>")

        Args:
            file_name (str): The filename including the file path

            data (str): Data you want to write to a file
        """
        print(TAG + str(os.getcwd()))

        IO.mkdir(os.path.dirname(file_name))

        file = open(file_name, "w")

        file.write(data)
        file.close()

    @staticmethod
    def read_file(file_name) -> str:
        """Method to read data string from a file

        Args:
            file_name (str): The file to read (including filepath)

        Returns:
            str: String from file
        """

        print(TAG + str(os.getcwd()))

        myfile = Path(file_name)
        print(myfile.cwd())
        if myfile.is_file():
            file = open(file_name, "r")

            output = file.read()

            return output
        else:
            return None

    @staticmethod
    def mkdir(path):
        """Makes the directory if it does not exist

        Args:
            path (str): The directory path to create
        """

        if path != '':
            try:
                os.makedirs(path, exist_ok=True)
            except OSError as e:
                if e.errno == errno.EEXIST and os.path.isdir(path):
                    pass
                else:
                    raise

    @staticmethod
    def is_dir(path) -> bool:
        return os.path.isdir(path)

    @staticmethod
    def parse_path(path):
        return os.path.dirname(path)

    @staticmethod
    def get_path_size(path):
        cmd = "du " + path + " | awk '{print $1}'"
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        outs = proc.communicate()

        print(outs[-1])
        return int(outs[-1])

    @staticmethod
    def switch_working_dir(path):
        """Switches directory

        Args:
            path (str): The directory path
        """
        os_path = os.path.dirname(path)
        IO.mkdir(os_path)
        os.chdir(os_path)


class ConfigModel:
    WORKING_DIR = "/home/pi/Documents/HDDBackup/"
    FILE_NAME = "config.cfg"

    def __init__(self):
        IO.switch_working_dir(self.WORKING_DIR)
        self.config = Config()

    def load(self):
        config_str = IO.read_file(self.FILE_NAME)
        if config_str is not None and not config_str == "":
            self.config.load_json_string(config_str)

        self.config.print()

    def save(self):
        config_str = self.config.to_json_string()
        IO.write_file(self.FILE_NAME, config_str)

        self.config.print()



