import os
import random
import string
import subprocess

from config.config import IO


class Utils:
    @staticmethod
    def generate_random(n):
        return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(n))

    @staticmethod
    def path_to_name(path: str):
        parts = path.split("/")
        return parts[-1]

    @staticmethod
    def umount(mount_path: str):
        cmd = 'sudo umount ' + mount_path

        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)

        output = proc.communicate()

        for line in output:
            print(str(line))

    @staticmethod
    def detach(device_node: str):
        cmd = 'udisks --detach ' + device_node
        output = Utils.subprocess_cmd(cmd)

        for out in output:  # type: bytes
            print(out.decode())

    @staticmethod
    def subprocess_cmd(command):
        """Runs system commands using subprocess

        Shorthand for writing to shell/command_line (return lines).

        Note:
            If running a command which does not end, there will be no output

        Args:
            command (str):

        Returns:
            str: Output from command
        """
        process = subprocess.Popen(command,
                                   stdout=subprocess.PIPE,
                                   shell=True)
        lines = process.stdout.readlines()
        return lines

    @staticmethod
    def sys_run(commands):
        """Runs system commands without using subprocess

        Use this if you don't need the output from command.
        Doesn't have the Shell that Popen has.
        Not recommended in general, but useful for quick cmds.

        Args:
            commands (list): command(s) to run
        """
        for cmd in commands:
            os.system(cmd)


