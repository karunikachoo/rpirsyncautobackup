# import eventlet
# eventlet.monkey_patch()

import os
import time
from time import sleep
import threading

from flask import Flask, render_template, make_response, session, request
from flask_socketio import SocketIO, emit, join_room, send

from auth.authenticator import Auth
from config.config import ConfigModel
from messages.handler import MessageHandler
from mount.discovery import Monitor
from mount.discovery2 import Discovery
from rsync.rsync_handler import RsyncHandler
from utils.utils import Utils


class Main(threading.Thread):
    WAIT = ['-', '\\', '|', '/']

    def __init__(self):
        super(Main, self).__init__()

        self.pos = 0

        self.model = ConfigModel()
        self.msg_handler = MessageHandler(app=app, main=self, socketio=socketio)

        self.auth = Auth()

        # self.discovery = Monitor(main=self)
        self.discovery = Discovery(main=self)
        self.rsync_handler = RsyncHandler(main=self)

    def run(self):
        """
        Initialisation
        """
        self.model.load()
        self.discovery.start()
        self.rsync_handler.start()

        """
        Main Loop
        """
        while True:
            self.print_wait()

            self.auth.check_tokens()
            sleep(0.5)

    def print_wait(self):
        if self.pos < len(self.WAIT):
            print(self.WAIT[self.pos], time.time(), end='\r')
            self.pos += 1
        else:
            self.pos = 0

    # def test_udev(self):
    #     context = pyudev.Context()
    #     devs = [device for device in context.list_devices(subsystem='block', DEVTYPE='disk') if device['MAJOR'] == '8']
    #     for device in devs:
    #         partitions = [device.device_node for device in context.list_devices(subsystem='block', DEVTYPE='partition', parent=device)]
    #
    #         disk_partitions = [p for p in psutil.disk_partitions() if p.device in partitions and len(partitions) > 0]
    #
    #         hdd = HDD(serial=device['ID_SERIAL'], device_node=device.device_node)
    #
    #         for p in disk_partitions:
    #
    #             hdd.add_partition(partition_str=p.device, mount_point=p.mountpoint)
    #
    #         hdd.get_size()
    #
    #         hdd.print()


app = Flask(__name__)
app.config['SECRET_KEY'] = Utils.generate_random(16)
socketio = SocketIO(app, async_mode='threading')
# socketio = SocketIO(app)
main = Main()


@app.route('/')
def auth():
    resp = make_response(render_template('signin.html'))
    resp.set_cookie('uid', main.auth.generate_token())

    return resp


@app.route('/index')
def index():
    resp = make_response(render_template('index.html'))

    return resp


@socketio.on('py-hello')
def py_hello(token):
    join_room(token)
    emit('py-hello', room=token)


@socketio.on('py-cmd')
def handle_cmd(json):
    main.msg_handler.handle(json)


def get_ssl_tuple():
    path = os.getcwd()
    chain = path + '/fullchain.pem'
    priv = path + '/privkey.pem'

    return chain, priv


if __name__ == "__main__":
    socketio.start_background_task(target=main.run)
    socketio.run(app=app, host="0.0.0.0", port=5000)
    # socketio.run(app=app, host="0.0.0.0", port=5000, ssl_context=get_ssl_tuple())
    # socketio.run(app=app, host="0.0.0.0", port=5000, ssl_context='adhoc')
    # app.run(debug=True, host="0.0.0.0", port=5000, use_reloader=False)

