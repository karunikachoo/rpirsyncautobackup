## RPi Rsync Auto Backup (RRAB)



What is RRAB? It was born out of a need to design an easy to use system, where you need only plug in your external hard disk drive (HDD) and the tool will handle the backup, unmount and power down of your external HDD.

---

## Overview
 
 **Glossary:**

| Term         | Meaning                      |
| -------------|------------------------------|
| Target drive | HDD to back up file **from** |
| Backup drive | HDD to back up file **to**   |

---
 
 **Typical Usage Scenario:**
 
 1. You have a Raspberry Pi connected to a powered USB hub, with an external HDD setup as the backup drive.
 2. You return from a long day's work/wherever, plug in your target external HDD and take a bath.
 3. If files are backed up often, when bath time is over, the target HDD should be unmounted and powered down.
 4. You unplug your target HDD and put it in your bag so you *don't forget it the next day*.
 
--- 

## Installation and Setup

Make sure these are installed on your raspberry pi:

* python3
* pip3
* rsync
* udisks
* umount
* ntfs-3g
* lsblk

---

Create a folder for the repo in `/home/pi`, i.e. `/home/pi/HDDBackup` 


Clone the repo into your raspberry pi using git:

	git clone git@bitbucket.org:karunikachoo/rpirsyncautobackup.git

`cd` into `HDDBackup` and install the required python modules:

	sudo pip3 install -r requirements.txt

Test the tool by running ```sudo python3 main.py```

Ensure that there are no errors, then stop the tool by pressing `ctrl` + `c` a few times.

Run `sudo python3 generate_pw.py {username} {password}` Replacing `{username}` and `{password}` accordingly.

i.e `sudo python3 generate_py.py admin admin`  

Run `main.py` again and with your computer/smartphone go to 
	
	http://{rpi's ip addr}:5000

You should see a login screen, enter your username and password and click `Sign In`

---

For more information please head over to the repo's [wiki](https://bitbucket.org/karunikachoo/rpirsyncautobackup/wiki/)